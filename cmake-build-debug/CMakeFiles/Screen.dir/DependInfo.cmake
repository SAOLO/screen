# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/fimbulg/screen/main.cpp" "/home/fimbulg/screen/cmake-build-debug/CMakeFiles/Screen.dir/main.cpp.o"
  "/home/fimbulg/screen/screen.cpp" "/home/fimbulg/screen/cmake-build-debug/CMakeFiles/Screen.dir/screen.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "DEBUG"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/fimbulg/Dev/Tools/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/fimbulg/screen/cmake-build-debug/terminal/CMakeFiles/Terminal-STATIC.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
