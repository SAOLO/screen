/*
 * Autor       : Mikael Nilsson
 * Filename    : main.cpp
 * Description : C++ Screen example
 * Version     : 0.2
 *
*/

#include <memstat.hpp>
#include "screen.h"
#include <iostream>

using namespace std;

int main()
{
    Terminal terminal;
    Screen screen(80, 24);
    //Första triangeln
    screen.fill(' ', TerminalColor(COLOR::WHITE, COLOR::BLUE));
    screen.set(39,1,' ', TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.set(38,2,' ', TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.set(40,2,' ', TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.set(41,3,' ', TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.set(37,3,' ', TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.set(36,4,' ', TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.set(35,5,' ', TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.set(42,4,' ', TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.set(43,5,' ', TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.set(34,6,' ', TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.set(33,7,' ', TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.set(44,6,' ', TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.set(45,7,' ', TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.fillRect(34,7,11,1,' ',TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.set(39,2,' ', TerminalColor(COLOR::DEFAULT, COLOR::YELLOW));
    screen.fillRect(38,3,3,1,' ',TerminalColor(COLOR::DEFAULT, COLOR::YELLOW));
    screen.fillRect(37,4,5,1,' ',TerminalColor(COLOR::DEFAULT, COLOR::YELLOW));
    screen.fillRect(36,5,7,1,' ',TerminalColor(COLOR::DEFAULT, COLOR::YELLOW));
    screen.fillRect(35,6,9,1,' ',TerminalColor(COLOR::DEFAULT, COLOR::YELLOW));

    //Andra triangeln
    screen.set(33,8,' ', TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.set(32,9,' ', TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.set(31,10,' ', TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.set(30,11,' ', TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.set(29,12,' ', TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.set(28,13,' ', TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.set(27,14,' ', TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.set(39,14,' ', TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.set(38,13,' ', TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.set(37,12,' ', TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.set(36,11,' ', TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.set(35,10,' ', TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.set(34,9,' ', TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.fillRect(26,14,13,1,' ',TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.set(33,9,' ', TerminalColor(COLOR::DEFAULT, COLOR::YELLOW));
    screen.fillRect(32,10,3,1,' ',TerminalColor(COLOR::DEFAULT, COLOR::YELLOW));
    screen.fillRect(31,11,5,1,' ',TerminalColor(COLOR::DEFAULT, COLOR::YELLOW));
    screen.fillRect(30,12,7,1,' ',TerminalColor(COLOR::DEFAULT, COLOR::YELLOW));
    screen.fillRect(29,13,9,1,' ',TerminalColor(COLOR::DEFAULT, COLOR::YELLOW));

    //Tredje triangeln
    screen.set(45,8,' ', TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.set(46,9,' ', TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.set(47,10,' ', TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.set(48,11,' ', TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.set(49,12,' ', TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.set(50,13,' ', TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.set(51,14,' ', TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.set(39,14,' ', TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.set(40,13,' ', TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.set(41,12,' ', TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.set(42,11,' ', TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.set(43,10,' ', TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.set(44,9,' ', TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.fillRect(40,14,13,1,' ',TerminalColor(COLOR::DEFAULT, COLOR::DEFAULT));
    screen.set(45,9,' ', TerminalColor(COLOR::DEFAULT, COLOR::YELLOW));
    screen.fillRect(44,10,3,1,' ',TerminalColor(COLOR::DEFAULT, COLOR::YELLOW));
    screen.fillRect(43,11,5,1,' ',TerminalColor(COLOR::DEFAULT, COLOR::YELLOW));
    screen.fillRect(42,12,7,1,' ',TerminalColor(COLOR::DEFAULT, COLOR::YELLOW));
    screen.fillRect(41,13,9,1,' ',TerminalColor(COLOR::DEFAULT, COLOR::YELLOW));

    screen.setTextRect(30,16,16,5,"It is the tri-  force from Zeldaif you can not  recognise it!", TerminalColor(COLOR::RED, COLOR::BLUE));

    screen.setText(30,22,"Bye bye!", TerminalColor(COLOR::RED,COLOR::BLUE));

    screen.draw(terminal);

    return 0;
}

